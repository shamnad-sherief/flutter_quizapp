import 'package:flutter/material.dart';

class QuestionsSummary extends StatelessWidget {
  const QuestionsSummary(this.summaryData, {super.key});

  final List<Map<String, Object>> summaryData;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: SingleChildScrollView(
        child: Column(
          children: summaryData.map((data) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ResultSummaryText(
                    '${((data['question_index'] as int) + 1).toString()}.'),
                const SizedBox(
                  width: 4,
                ),
                Column(
                  children: [
                    ResultSummaryText(data['question'] as String),
                    const SizedBox(
                      height: 4,
                    ),
                        ResultSummaryText(data['user_answer'] as String),
                    const SizedBox(
                      height: 4,
                    ),
                    ResultSummaryText(data['question_answer'] as String),
                    const SizedBox(
                      height: 4,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            );
          }).toList(),
        ),
      ),
    );
  }
}

class ResultSummaryText extends StatelessWidget {
  const ResultSummaryText(this.centerText, {super.key});

  final String centerText;

  @override
  Widget build(BuildContext context) {
    return Text(
      centerText,
      style: const TextStyle(
        color: Colors.white,
      ),
    );
  }
}
