import 'package:quiz_app/models/quiz_questions.dart';

const questions = [
  QuizQuestion(
    'What is the color of sky?',
    [
      'Blue',
      'Green',
      'White',
    ],
  ),
  QuizQuestion(
    'How many colors does a rainbow has?',
    [
      '7',
      '3',
      '1',
    ],
  ),
  QuizQuestion(
    'What comes after 5?',
    [
      '6',
      '4',
      '2',
    ],
  )
];
